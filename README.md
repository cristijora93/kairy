Introduction
============

Tastier Restaurant Front end SPA

Installation
------------

#### Download:

Download from BitBucket

```
git clone https://bitbucket.org/cristijora93/kairy
```

#### Requirements:

* NodeJs installed

* MongoDB installed and running

#### Build Setup

``` bash
# Go to membership folder and install dependencies

npm install

# build membership module
npm run build

# Go to webapi folder and install dependencies
npm install

# Run locally with hot reload
npm run dev

```

Documentation
-------------
Depending on the component you are working on you should have the following links handy.
- NodeJs - [online documentation](https://nodejs.org/en/)
- Express - [online documentation](http://expressjs.com/)
- MongoDB - [online documentation](https://www.mongodb.com/)